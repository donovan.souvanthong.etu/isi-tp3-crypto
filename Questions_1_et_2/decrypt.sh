#!/bin/bash

#Decrypt

#Reconstruction de keymaster
openssl enc -d -aes-256-cbc -in cle-usb-2/part00.pem.enc -out ./ramdisk/part00.pem -pass file:./cle-usb-1/key1.pem
openssl enc -d -aes-256-cbc -in cle-usb-1/part01.pem.enc -out ./ramdisk/part01.pem -pass file:./cle-usb-2/key2.pem
cat ./ramdisk/part00.pem ./ramdisk/part01.pem > ./ramdisk/keymaster.pem

for file in ./disk/*.enc; do
	openssl rsautl -decrypt -inkey ./ramdisk/keymaster.pem -in $file -out $file.txt
	mv -- $file.txt ${file%.enc}.txt
	rm $file
	echo "File decrypted successfully: \n" && cat ${file%.enc}.txt
done

rm ./ramdisk/keymaster.pem ./ramdisk/part00.pem ./ramdisk/part01.pem

