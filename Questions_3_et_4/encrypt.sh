#!/bin/bash

#Encrypt

openssl genrsa 2048 > ./ramdisk/keymaster.pem

for file in ./disk/*.txt; do
	openssl rsautl -encrypt -in $file -inkey ./ramdisk/keymaster.pem -out $file.enc
	mv -- $file.enc ${file%.txt}.enc

	rm $file
	echo "File $file encrypted successfully"
done

#For MacOS
gsplit -l 14 -d --additional-suffix=.pem ./ramdisk/keymaster.pem ./ramdisk/part

#For Linux
#split -l 14 -d --additional-suffix=.pem ./ramdisk/keymaster.pem ./ramdisk/part

openssl enc -aes-256-cbc -salt -in ./ramdisk/part00.pem -out ./cle-usb-2/part00.pem.encByKey1 -pass file:./cle-usb-1/key1.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/part00.pem -out ./cle-usb-2/part00.pem.encByKey3 -pass file:./cle-usb-3/key3.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/part01.pem -out ./cle-usb-1/part01.pem.encByKey2 -pass file:./cle-usb-2/key2.pem
openssl enc -aes-256-cbc -salt -in ./ramdisk/part01.pem -out ./cle-usb-1/part01.pem.encByKey4 -pass file:./cle-usb-4/key4.pem
cp ./cle-usb-1/part01.pem.encByKey2 ./cle-usb-3/
cp ./cle-usb-1/part01.pem.encByKey4 ./cle-usb-3/
cp ./cle-usb-2/part00.pem.encByKey1 ./cle-usb-4
cp ./cle-usb-2/part00.pem.encByKey3 ./cle-usb-4

rm ./ramdisk/part00.pem ./ramdisk/part01.pem ./ramdisk/keymaster.pem


