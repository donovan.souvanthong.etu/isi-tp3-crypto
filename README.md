# ISI-TP3-CRYPTO

## Binome:
- SOUVANTHONG Donovan
- ROCQ Thomas

### Question 1 & 2

La solution de chiffrement/déchiffrement à 2 facteurs que nous avons choisi est la suivant: 
Nous utilisons 2 clés USB qui seront nos 2 responsables, un disk qui contient le fichier chiffré/déchiffré, un ramdisk vide, qui servira de passerelle de stockage entre nos clés et notre disk. 
La méthode que nous utilisons et que nous allons vous décrire plus bas repose sur le fait de diviser en 2 parties la clé permettant de chiffer/déchiffrer les données (ici **file**)

---

Voici la topologie que nous avons par défaut:

```mermaid
graph TD;
subgraph Disk;
file;
end;
subgraph RamDisk;
A{Empty};
end;
subgraph USB Key 1;
Key1;
end;
subgraph USB Key 2;
Key2;
end;
```

Nous avons 2 clés USB, chacune contenant une clé différente, un ramdisk vide pour l'instant, ainsi qu'un disk contenant nos données non chiffrées **file**

---

A l'exécution du script **encrypt.sh**, voici ce qu'il se passe:

```mermaid
graph TD
subgraph Disk
file
crypt
end
subgraph RamDisk
keymaster
part01
part00
end
subgraph USB Key 1
Key1
part01.enc
end
subgraph USB Key 2
Key2
part00.enc
end
keymaster -- Encrypt --> file
file --> crypt
keymaster -- Split --> part00
keymaster -- Split --> part01
Key1 -- Encrypt --> part00
part01 --> part01.enc
Key2 -- Encrypt --> part01
part00 --> part00.enc
```

- Une clé **keymaster** est générée.
- Nous utilisons la clé **keymaster** pour chiffrer notre fichier **file** et nous obtenons le fichier **crypt**.
- Nous **divisons** ensuite keymaster en 2 parties **part00** et **part01**.
- Nous chiffrons **part00** avec **key2** (clé USB 2) et stockons le chiffré obtenu **part00.enc** dans la clé USB 1.
- Nous chiffrons **part01** avec **key1** (clé USB 1) et stockons le chiffré obtenu **part01.enc** dans la clé USB 2.
- Nous vidons ensuite le ramdisk en supprimant **keymaster**, **part00** et **part01**.
- Enfin nous supprimons le fichier non encrypté **file** pour ne laisser dans le disk que le fichier crypté.

---

Voici donc la topologie obtenue après avoir exécuté le script **encrypt.sh**:

```mermaid
graph TD
subgraph Disk
crypt
end
subgraph RamDisk
A{Empty};
end
subgraph USB Key 1
Key1
part01.enc
end
subgraph USB Key 2
Key2
part00.enc
end
```

---

A l'exécution du script **decrypt.sh**, voici ce qu'il se passe:

```mermaid
graph LR
subgraph Disk
crypt
file
end
subgraph RamDisk
keymaster
part00
part01
end
subgraph USB Key 1
Key1
part01.enc
end
subgraph USB Key 2
Key2
part00.enc
end
Key2 -- Decrypt --> part01.enc
part01.enc --> part01
Key1 -- Decrypt --> part00.enc
part00.enc --> part00
part00 -- Rebuild --> keymaster
part01 -- Rebuild --> keymaster
keymaster -- Decrypt --> crypt
crypt --> file
```

- On utilise **key1** (clé USB 1) pour décrypter **part01.enc** (clé USB 2) et nous stockons la partie déchiffrée **part01** dans **ramdisk**
- On utilise **key2** (clé USB 2) pour décrypter **part00.enc** (clé USB 1) et nous stockons la partie déchiffrée **part00** dans **ramdisk**
- On reconstruit ensuite la clé **keymaster** en concaténant les 2 parties **part00** et **part01**
- Enfin on décrypte le fichier **crypt** avec la clé **keymaster** pour obtenir le nom chiffré **file**

---

Voici donc la topologie obtenue après avoir exécuté le script **decrypt.sh**:
```mermaid
graph TD
subgraph Disk
file
end
subgraph RamDisk
A{Empty};
end
subgraph USB Key 1
Key1
part01.enc
end
subgraph USB Key 2
Key2
part00.enc
end
```

---

### Question 3 & 4

Pour faire évoluer la solution afin que les représentants puissent se substituer aux responsables, nous allons introduire **2 nouvelles** clés USB qui corresponderont donc aux représentants. Ensuite, durant le processus d'encryptage, nous allons toujours encrypter les **2 parties** de la **keymaster**, mais nous allons cette fois-ci le faire **2 fois** afin que le représentant et son responsable puissent chacun décrypter la partie avec **sa clé**. 

On a donc 2 nouvelles clés **cle-usb-3** (le représentant de **cle-usb-1** pouvant se substituer à lui) et **cle-usb-4** (le représentant de **cle-usb-2** pouvant se substituer à lui).

La procédure sera donc la même que pour les questions 1 et 2, nous allons encroûter les données avec une clé **keymaster** qui sera ensuite coupée en 2 et **encryptée** elle même avant d'être stockée dans les clés.

Pour ce faire, nous allons maintenant créer **2 fichiers cryptés** pour chaque partie de la keymaster. On aura donc la part00 qui sera encryptée une fois par la clé 1, puis une deuxième fois par la clé 3, et la part01 qui sera encryptée une fois par la clé 2, puis une deuxième fois par la clé 4.

Ainsi, le fichier crypté sera décryptable en utilisant les combinaisons de clés suivantes:

- Clé USB 1 + clé USB 2
- Clé USB 1 + clé USB 4
- Clé USB 3 + clé USB 2
- Clé USB 3 + clé USB 4

---

Voici la topologie que nous avons par défaut:

```mermaid
graph TD;
subgraph Disk;
file;
end;
subgraph RamDisk;
A{Empty};
end;
subgraph USB Key 1;
Key1;
end;
subgraph USB Key 2;
Key2;
end;
subgraph USB Key 3;
Key3;
end;
subgraph USB Key 4;
Key4;
end;
```
  
Nous avons 2 clés USB, chacune contenant une clé différente, un ramdisk vide pour l'instant, ainsi qu'un disk contenant nos données non chiffrées **file**

---

A l'exécution du script **encrypt.sh**, voici ce qu'il se passe:


```mermaid
graph TD
subgraph Disk
file
crypt
end
subgraph RamDisk
keymaster
part01
part00
end
subgraph USB Key 1
Key1
part01.encbyKey2
part01.encbyKey4
end
subgraph USB Key 2
Key2
part00.encByKey1
part00.encByKey3
end
subgraph USB Key 3
Key3
part01.encbyKey2.2
part01.encbyKey4.2
end
subgraph USB Key 4
Key4
part00.encByKey1.2
part00.encByKey3.2
end
keymaster -- Encrypt --> file
file --> crypt
keymaster -- Split --> part00
keymaster -- Split --> part01
Key1 -- Encrypt --> part00
part01 --> part01.encbyKey2
Key2 -- Encrypt --> part01
part00 --> part00.encByKey1
part01.encbyKey2 -- Copy --> part01.encbyKey2.2
part01.encbyKey4 -- Copy --> part01.encbyKey4.2
part00.encByKey1 -- Copy --> part00.encByKey1.2
part00.encByKey3 -- Copy --> part00.encByKey3.2
Key3 -- Encrypt --> part00
part00 --> part00.encByKey3
Key4 -- Encrypt --> part01
part01 --> part01.encByKey4
```  

- Une clé **keymaster** est générée.
- Nous utilisons la clé **keymaster** pour chiffrer notre fichier **file** et nous obtenons le fichier **crypt**.
- Nous **divisons** ensuite keymaster en 2 parties **part00** et **part01**.
- Nous chiffrons **part00** avec **key2** (clé USB 2) et stockons le chiffré obtenu **part00.encByKey2** dans la clé USB 1.
- Nous chiffrons **part00** avec **key4** (clé USB 2) et stockons le chiffré obtenu **part00.encByKey4** dans la clé USB 1.
- Nous copions **part00.encByKey2** dans la clé USB 3
- Nous copions **part00.encByKey4** dans la clé USB 3
- Nous chiffrons **part01** avec **key1** (clé USB 1) et stockons le chiffré obtenu **part01.encByKey1** dans la clé USB 2.
- Nous chiffrons **part01** avec **key3** (clé USB 1) et stockons le chiffré obtenu **part01.encByKey3** dans la clé USB 2.
- Nous copions **part01.encByKey1** dans la clé USB 4
- Nous copions **part01.encByKey2** dans la clé USB 4
- Nous vidons ensuite le ramdisk en supprimant **keymaster**, **part00** et **part01**.
- Enfin nous supprimons le fichier non encrypté **file** pour ne laisser dans le disk que le fichier crypté.


---

Voici la topologie que nous avons après avoir exécuté le script **encrypt.sh**:

```mermaid
graph TD;
subgraph Disk;
crypt;
end;
subgraph RamDisk;
A{Empty};
end;
subgraph USB Key 1;
Key1;
part01.encbyKey2
part01.encbyKey4
end;
subgraph USB Key 2;
Key2;
part00.encbyKey1
part00.encbyKey3
end;
subgraph USB Key 3;
Key3;
part01.encbyKey2.2
part01.encbyKey4.2
end;
subgraph USB Key 4;
Key4;
part00.encbyKey1.2
part00.encbyKey3.2
end;
```

---


Nous allons maintenant exécuter le script **decrypt.sh** avec les clés **1 et 4** insérée, voici ce qu'il se passe:


```mermaid
graph LR
subgraph Disk
crypt
file
end
subgraph RamDisk
keymaster
part00
part01
end
subgraph USB Key 1
Key1
part01.encByKey2
part01.encByKey4
end
subgraph USB Key 4
Key4
part00.encByKey1
part00.encByKey3
end
Key4 -- Decrypt --> part01.encByKey4
part01.encByKey4 --> part01
Key1 -- Decrypt --> part00.encByKey1
part00.encByKey1 --> part00
part00 -- Rebuild --> keymaster
part01 -- Rebuild --> keymaster
keymaster -- Decrypt --> crypt
crypt --> file
```

- On vérifie tout d'abord que la combinaison des 2 clés **fonctionne**, ici on a les clés **1 et 4** qui permettent à 2 de déchiffrer le fichier.
- On utilise **key1** (clé USB 1) pour décrypter **part01.encByKey1** (clé USB 4) et nous stockons la partie déchiffrée **part01** dans **ramdisk**
- On utilise **key4** (clé USB 2) pour décrypter **part00.encByKey4** (clé USB 1) et nous stockons la partie déchiffrée **part00** dans **ramdisk**
- On reconstruit ensuite la clé **keymaster** en concaténant les 2 parties **part00** et **part01**
- Enfin on décrypte le fichier **crypt** avec la clé **keymaster** pour obtenir le nom chiffré **file**
Notons que les parties encryptées par les clés **2 et 3** ne sont pas utilisées ici, car ces clés ne sont pas insérées.

---

Voici la topologie que nous avons après avoir exécuté le script **decrypt.sh**:


```mermaid
graph TD;
subgraph Disk;
file;
end;
subgraph RamDisk;
A{Empty};
end;
subgraph USB Key 1;
Key1;
part01.encbyKey2
part01.encbyKey4
end;
subgraph USB Key 2;
Key2;
part00.encbyKey1
part00.encbyKey3
end;
subgraph USB Key 3;
Key3;
part01.encbyKey2.2
part01.encbyKey4.2
end;
subgraph USB Key 4;
Key4;
part00.encbyKey1.2
part00.encbyKey3.2
end;
```

### Question 5 & 6

Pour la révocation des droit, nous utilisons la même méthode et la même topologie que pour la partie 2, mais cette fois-ci nous passons en variables les numéros des clés à utiliser pour le chiffrage. 
Ainsi, si un responsable ou un représentant est démis de ses fonctions, il faudra simplement déchiffrer toutes les données présentes sur le disque, puis les chiffrer à nouveau en utilisant la clé de la personne qui remplacera la personne qui a été démise de ses fonctions.
